1. normal flow
   -> git clone <ssh/https>
   -> git branch <branch-name>
   -> git checkout <branch-name>
   -> git add .
   -> git commit -m "commit message"
   -> git push origin <branch name>


2. Merge conflect
    -> git clone <ssh/https>
    -> git branch <branch-name>
    -> git checkout <branch-name>
    -> git add .
    -> git commit -m "commit message"
    -> git push orgin main
          -> error that lagging some commits
resolution:
    -> git pull
    -> if there is a conflicts we have to modifi it manually
    -> git add .
    -> git commit 
    -> git push origin main 
	
3. git rebase
    -> git clone 
    -> git branch <branch name>
    -> git checkout <branch name>
       made some changes
    -> git add .
    -> git commit -m "commit message"
    -> git pull --rebase origin main   ---> updating the local repo with rebase command
       if no conflects rebased with latest. if you get any conflicts, resolve it manually. Once done.
    -> git rebase --continue.

4. stash
    -> git stash
    -> git stash list --> will show the stash id'show
    -> git stash apply {stash-id}

5. delete a branch
   -> git branch -d <branch name>
   -> git branch -D <branch name> -> for force delete
   
6. git cherry-pick
   -> If we want to get the changes from the committed branch to another branch (ex main(master))
   -> git cherry-pick <commit id in the remote branch>
   -> After this the changes will automatically added to the branch which we were in. So, no need to do add or commit.
   -> git push origin <your current branch>
   

For reference go through the below link try it out

https://git-scm.com/docs
