#include <stdio.h>

int a = 20;
int *global = &a;
void memory(int **pp)
{
 int b = 50;
 *pp = &b;
 printf("in function the value of *pp is %d\n",**pp);
}
int main()
{
 //int a = 20;
 //int *p = &a;

 printf("the value of *p = %d before function call\n",*global); 
 memory(&global);

 printf("the value of *p after function call is %d\n",*global);
}
