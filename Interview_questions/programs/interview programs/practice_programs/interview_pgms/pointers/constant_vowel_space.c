#include <stdio.h>
#include <stdlib.h>
#include <ctype.h> 

void scan_lines(char (*line)[10], int *pv, int *pc, int *pd, int *pw, int *po)
{
  char c;
 int count =0;
  for (int i = 0; i < 4; i++)
  {
  while((c = toupper(*(*(line + i)+count))) != '\0')        
  {   
    if (c == 'A' || c == 'E' || c == 'I' || c == 'O' || c == 'U')
	    ++ *pv;
    else if (c >= 'A' && c <= 'Z')
	    ++ *pc;
    else if (c >= '0' && c <= '9')
	    ++ *pd;
    else if (c == ' ' || c == '\t')
	    ++ *pw;
    else 
	    ++ *po;

    ++count;
   }
  count = 0;
   }
}

int main()
{
 char line[4][10] = {0};
 int vowels = 0;
 int consonents = 0;
 int digits = 0;
 int whitespc = 0;
 int other = 0;

 printf("Enter a line of text below\n");
 for (int i = 0; i < 4; i++)
 scanf("%s",line[i]);

 scan_lines(line, &vowels, &consonents, &digits, &whitespc, &other);

 printf("\nnumber of vowels = %d",vowels);
 printf("\nnumber of consonents = %d",consonents);
 printf("\nnumber of digits = %d",digits);
 printf("\nnumber of whitespc = %d",whitespc); 
 printf("\nnumber of others = %d",other);
}
