#include<stdio.h>
#include<stdlib.h>

void print(int **arr, int m, int n)
{
    int i, j;
    for (i = 0; i < m; i++)
    {
      for (j = 0; j < n; j++)
      {
    printf("%d ",*(arr+i*n+j));                   //print the elements of the matrix
    //printf("%d ", *((arr+i*n) + j));
      }
      printf("\n");
    }
}


int main()
{
    int arr[20][20],m,n,c,d;
    printf("row=\n");
    scanf("%d",&m);
    printf("col=\n");
    scanf("%d",&n);
    for(c=0;c<m;c++)
    {
     for(d=0;d<n;d++)
      {
       scanf("%d",&arr[c][d]);
      }
    }
    for(c=0;c<m;c++)                      //print the matrix without function calling
    {
     for(d=0;d<n;d++)
     {
     printf("%d ",arr[c][d]);
     }
     printf("\n");
    }
    print((int **)arr, m, n);            //print the matrix using function calling
    return 0;
}
