#include <stdio.h>
#include <string.h>
#include <stdlib.h>

char change_string(char *string)
{
   char a[] = "green";
   for(int i = 0; i < strlen(a); i++)
      *(string+i) = a[i];
}

int main()
{
  char string[10] = "red";

  printf("1) %s\n",string);

  change_string(string);

  printf("2) %s\n",string);
}
