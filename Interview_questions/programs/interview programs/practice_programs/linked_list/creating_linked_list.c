#include <stdio.h>
#include <stdlib.h>
#include <malloc.h>


  struct node
  {
    int num;
    struct node *link;
  };
  typedef struct node NODE;
 void listing_the_even_odd(NODE **head);

void main()
{
  NODE *head, *first, *temp = 0;
  int count = 0;
  int choice = 1;
  first = 0;

  while (choice)
  {
    head = (NODE *)malloc(sizeof(NODE));
    printf("Enter the data iteam\n");
    scanf("%d",&head->num);

    if(first != 0 )
    {
      temp->link = head;
      temp = head;
    }
    else
    {
      first = temp = head;
    }
    fflush(stdin);
    printf("Do you want continue (Type 0 or 1)?\n");
    scanf("%d",&choice);
  }


  /* after creation of the list we have to assign the last link to zero */

  temp->link = 0;

  temp = first;

  /* sorting the linked list to odd & even */
  listing_the_even_odd(&temp);

  /* reset temp to the beginning */

  temp = first;
  printf("\n status of the linked list is \n");

  /* This is used for printing the list */
  while(temp != 0)
  {
    printf("%d=>",temp->num);
    count++;
    temp = temp->link;
  }
  printf("NULL\n");
  printf("No. of nodes in the list = %d\n",count);


}


 /* listing the even and odd in a sorting order */

   void listing_the_even_odd(NODE **head)
  {
    NODE *end = *head;
    NODE *prev = NULL;
    NODE *curr = *head;

    while (end->link != NULL)
    {
      end = end->link;
    }

    NODE *new_end = end;

    while (curr->num % 2 != 0 && curr != end)
    {
	    new_end->link = curr;
	    curr = curr->link;
	    new_end->link->link = NULL;
	    new_end = new_end->link;
    }

    if(curr->num %2 == 0)
    {
	*head = curr;

	while (curr != end)
	{
		if ((curr->num)%2 == 0)
		{
			prev = curr;
			curr = curr->link;
		}
		else
		{
			prev->link = curr->link;
			curr->link = NULL;
			new_end->link = curr;
			new_end = curr;
			curr = prev->link;
		}
	}
    }

    else{
       prev = curr;
    }

    if (new_end != end && (end->num)%2 != 0)
    {
	    prev->link = end->link;
	    end->link = NULL;
	    new_end->link = end;
    }
  }


