#include <stdlib.h>

int main() {
   /*
   ** Code snippet.
   */
   if(0 == system("pidof -x chrome > /dev/null")) {
      //A process having name PROCESS is running.
   }
   else if(1 == system("pidof -x chrome > /dev/null")) {
      //A process having name PROCESS is NOT running.
   }

   /*
   ** Continue processing further.
   */
}
