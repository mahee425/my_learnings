#include <stdio.h>

int addition(int a,int b,int *c)
{
	 *c =  a + b;
	// printf("in function the value of c = %d\n",*c);
}

int subtraction(int a,int b,int *c)
{
	*c = a - b;
}

int multiplication(int a,int b,int *c)
{
	*c = a * b;
}

void main()
{
	int a=10,b=20,c,i;
	int (*func_pointer[])(int a,int b,int *c) = {addition,subtraction,multiplication};
    	printf("enter the number 0->addition,1->sub,2->multiplication\n");
    	scanf("%d",&i);

    	(*func_pointer[i])(a,b,&c);

    	printf("the value is  %d\n",c);
}
