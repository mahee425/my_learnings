#include <stdio.h>

static int i; // initialization  is "ok"

i = 25; //----------> this gives an error that multiple initialization 

int main()
{
	i = 25;   // this is not an error, mean inside function we can initialize it you want.
 printf("%d",i);
}
