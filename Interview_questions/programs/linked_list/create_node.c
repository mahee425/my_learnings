#include <stdio.h>
#include <stdlib.h>

typedef struct node {
	int a;
	struct node *next;
}list;

list *head = NULL;


int create_node(int n)
{
	list *temp = NULL, *newNode = NULL;
	int i,data = 0;

	head = (list *)malloc(1*(sizeof(list)));

	if (head == NULL)
	{
		printf("memory allocation for head is failed\n");
	}
	
	printf("enter the data for node 1:\n");
	scanf("%d",&data);

	head->a = data;
	head->next = NULL;

	temp = head;

	for (i = 2; i <= n; i++)
	{
		newNode = (list *)malloc(1*(sizeof(list)));

		if(newNode == NULL)
		{
			printf("memory is not allocated for newNode\n");
		}

		printf("enter the data for node %d:\n",i);
		scanf("%d",&data);

		newNode->a = data;
		newNode->next = NULL;

		temp->next = newNode;
		temp = temp->next;
	}

}

int display_node(list *head)
{
	list *temp = NULL;

	temp = head;
	int i = 1;

	while(temp != NULL)
	{
		printf("node%d data: %d\n",i,temp->a);
		temp = temp->next;
		i++;
	}

}


int main(void)
{
	int n = 0;

	printf("enter the number of nodes to be created\n");
	scanf("%d",&n);

	create_node(n);

	display_node(head);
	return 0;
}
